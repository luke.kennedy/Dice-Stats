package main

import (
	"fmt"
	"io/fs"
	"io/ioutil"
	"log"
	"math"
	"math/rand"
	"runtime"
	"strings"
	"sync"

	"github.com/google/uuid"
)

const rollsTotal = 100000
const maxBufferSize = 100000
const dieSize = 10

type rollSet struct {
	size      int
	exploding bool
	results   map[uint16]float64
}

var sizes = []int{2, 4, 6, 8, 10, 12, 20}

func main() {
	workers := runtime.NumCPU()
	log.Printf("Spawning %d workers\n", workers)
	actualTotalRolls := rollsTotal
	rollsPerWorker := rollsTotal / workers
	if rollsTotal%workers != 0 {
		rollsPerWorker++
		actualTotalRolls = rollsTotal + workers - (rollsTotal % workers)
	}
	bufferSize := int(math.Min(rollsTotal, maxBufferSize))
	rollSets := []*rollSet{}
	for _, size := range sizes {
		rollSets = append(rollSets, &rollSet{
			size:      size,
			exploding: true,
			results:   getStats(uint16(size), true, workers, rollsPerWorker, actualTotalRolls, bufferSize),
		}, &rollSet{
			size:      size,
			exploding: false,
			results:   getStats(uint16(size), false, workers, rollsPerWorker, actualTotalRolls, bufferSize),
		})
	}
	maxOutcome := uint16(0)
	for _, rolls := range rollSets {
		for key := range rolls.results {
			if key > maxOutcome {
				maxOutcome = key
			}
		}
	}
	w := &strings.Builder{}
	w.WriteString("Result")
	for _, rolls := range rollSets {
		w.WriteString("," + name(rolls.size, rolls.exploding))
	}
	for i := uint16(1); i <= maxOutcome; i++ {
		w.WriteString(fmt.Sprintf("\n%d", i))
		for _, rolls := range rollSets {
			w.WriteString(fmt.Sprintf(",%g", rolls.results[i]))
		}
	}
	id := uuid.New()
	ioutil.WriteFile(id.String()+".csv", []byte(w.String()), fs.ModeExclusive)
	fmt.Println(w)
}

func name(size int, exploding bool) string {
	prefix := "Non-exploding "
	if exploding {
		prefix = "Exploding "
	}
	return fmt.Sprintf("%s%d", prefix, size)
}

func getStats(size uint16, exploding bool, workers, rollsPerWorker, totalRolls, bufferSize int) map[uint16]float64 {
	results := make(chan uint16, bufferSize)
	wg := &sync.WaitGroup{}
	wg.Add(workers)
	go func() {
		// Close all channels once the workers are done
		wg.Wait()
		close(results)
	}()
	for i := 0; i < workers; i++ {
		go func() {
			for j := 0; j < rollsPerWorker; j++ {
				if exploding {
					results <- rollExploding(size)
				} else {
					results <- uint16(rand.Int63n(int64(size))) + 1
				}
			}
			wg.Done()
		}()
	}
	stats := map[uint16]uint64{}
	for result := range results {
		stats[result] = stats[result] + 1
	}
	statsPercent := map[uint16]float64{}
	for key, val := range stats {
		statsPercent[key] = float64(val) / float64(totalRolls)
	}
	return statsPercent
}

func rollExploding(size uint16) uint16 {
	if size == 0 {
		panic("dice can't have zero faces")
	}
	res := uint16(rand.Int63n(int64(size))) + 1
	if res == size {
		res += rollExploding(size)
	}
	return res
}
